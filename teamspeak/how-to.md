# Teamspeak

This docker-compose.yaml creates a single Teamspeak container which stores its data inside a *data* folder located in the same directory as the docker-compose.yaml file. This docker-compose.yaml can be used right out of the box as there 
is no need for any changes.
To start a container, please execute `docker-compose up -d`.
