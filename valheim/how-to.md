# Valheim

This is docker-compose.yaml creates a single Valheim container which stores its data inside a *data* folder and its config inside a *config* folder, both located in the same directory as the docker-compose.yaml file.

## How-To

To get this Valheim server running, you need to fill in some values for the following three environment variables inside the docker-compose.yaml:
- SERVER_NAME: Your server name
- WORLD_NAME: The name of your world (name of your save-file)
- SERVER_PASS: The server password

Checkout the [valheim-server-docker](https://github.com/lloesche/valheim-server-docker#environment-variables) repository for more information about the image itself as well as more environment variables to customize the server even further.
Feel free to change the current pre-set environment variables depending on your requirements.

