# Dockerfile Collection
This repo is intended to store and share docker-compose files, dockerfiles and other infrastructure as code which I am using for my setup. If you see any errors or room for improvement, feel free to message me.

## Content

* TeamSpeak 3 Server
* Website + Nextcloud Setup
* Valheim Server
* Heimdall Server
* Dillinger Server

## License

docker-collection
Copyright (C) 2022-2024  ErikWe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 

