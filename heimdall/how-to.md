# Heimdall

This docker-compose.yaml creates a single Heimdall container which stores its data inside a *config* folder located in the same directory as the docker-compose.yaml file. This docker-compose.yaml can be used right out of the box as 
there is no need for any changes.
To create and start a container, please execute `docker-compose up -d`.
